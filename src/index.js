import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import injectTapEventPlugin from "react-tap-event-plugin";
injectTapEventPlugin();

import store from "./store";


ReactDOM.render(
    <MuiThemeProvider>
        <App />

    </MuiThemeProvider>
    ,
    document.getElementById('root')
);



	// <Provider store={store}>
	// </Provider>
