import React, {Component} from "react";
import Nav from "../Nav";
import "./projects.css";
import RaisedButton from "material-ui/RaisedButton";
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';


export default class Projects extends Component {

    render() {
        return (
            <div>
                <Nav/>
                <div className="content-projects">
                    <h2>projects</h2>
                    <div clasName="projects-list">
                        <Card>
                            <CardHeader title="Python Project!"/>
                            <CardText className="project-desc">
                                <img clasName="project-image" style={{height:'200px','max-width':'300px'}} src="https://dummyimage.com/300x200/000/fff.png"/>
                                <div className="project-info line-left">
                                    <p>hello</p>
                                </div>
                            </CardText>

                        </Card>
                    </div>
                </div>
            </div>
        );
    }
}
