import React, {Component} from "react";
import Nav from "../Nav";
import "./login.css";
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import {FlatButton} from 'material-ui/FlatButton';
import RaisedButton from "material-ui/RaisedButton";

import TextField from "material-ui/TextField";

import {Link} from "react-router-dom";


export default class Login extends Component {
// style={{margin:'2em auto',padding: '3em 3em 4em'}}
    render() {
        return (
            <div>
                <Nav/>
                <div className="content content-login">
                    <Card style={{margin:'2em auto'}}>
                        <CardHeader title="Login!" style={{'paddingBottom':'0px'}}/>
                        <CardText style={{'paddingTop':'1px'}}>
                            <TextField
                              id="text-field-default"
                              floatingLabelText="Email"
                            />
                            <br/>
                            <TextField
                              id="text-field-default"
                              floatingLabelText="Password"
                            />
                            <br />
                            <RaisedButton label="Login" primary={true} style={{width:'100%'}}/>
                        </CardText>
                    </Card>
                    <p>This is login</p>

                </div>
            </div>
        );
    }
}
